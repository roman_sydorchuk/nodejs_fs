const express = require('express');
const fs = require('fs');

const PORT = 8080
const filesDir = './files/';
const app = express();

const messages = {
    success: {
        createFile: {
            "message": "File created successfully"
        },
        getList(filesList) {
            return {
                massage: 'Success',
                files: filesList
            }
        },
        getFileInfo(filename, content, ext, birthtime) {
            return {
                message: "Success",
                filename: filename,
                content: content,
                extension: ext,
                uploadedDate: birthtime
            }
        }
    },
    badRequest: {
        createFile: {
            "message": "Please specify 'content' parameter"
        },
        getList: {
            "message": "Client error"
        },
        getFileInfo(filename){
            return {
                "message": `No file with '${filename}' filename found`
              }
        }
    },
    serverError: {
        "message": "Server error"
    }
}

function getListOfFilesInDir(directory) {
    return fs.readdirSync(directory).map(file => {
        return file;
    });;
}

app.use(express.json())

app.route('/api')
    //Create file with params
    .post((req, res) => {
        const dataFromRequest = req.body;
        try {
            fs.writeFileSync(`${filesDir}${dataFromRequest.filename}`, dataFromRequest.content);
            res.status(200).send(messages.success.createFile);
        } catch (error) {
            res.send(messages.badRequest.createFile);
        }
        res.status(500).send(messages.serverError);
    })
    //Get list of all uploaded files as array of filenames
    .get((req, res) => {
        try {
            res.send(messages.success.getList(getListOfFilesInDir(filesDir)))
        } catch (error) {
            res.send(messages.badRequest.getList);
        }
        res.status(500).send(messages.serverError);
    });
//Get file detailed info by specified filename
app.get('/api/files/:filename', (req, res) => {
    const filename = req.params.filename;
    const fileNamesList = getListOfFilesInDir(filesDir);
    if (fileNamesList.includes(filename)) {
        const birthtime = fs.statSync(`${filesDir}${filename}`).birthtime
        const extension = filename.substring(filename.indexOf('.'));
        const content = fs.readFileSync(`${filesDir}${filename}`,'utf8');
        res.send(messages.success.getFileInfo(filename, content, extension, birthtime))
    }else{
        res.send(messages.badRequest.getFileInfo(filename));
    }
    res.status(500).send(messages.serverError);
})
app.listen(PORT, () => {
    console.log(`Server is listening on port ${PORT}`);
})